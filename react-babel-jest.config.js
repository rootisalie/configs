const config = require('./babel-jest.config');

module.exports = {
  ...config,
  presets: [
    ...config.presets,
    '@babel/preset-react'
  ]
};
