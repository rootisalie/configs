module.exports = {
  moduleNameMapper: {
    '\\.scss$': 'identity-obj-proxy'
  },
  setupFiles: ['<rootDir>/node_modules/@fire-offer/configs/enzime.config']
};
)